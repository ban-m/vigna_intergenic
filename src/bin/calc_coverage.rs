extern crate bio;
extern crate rust_htslib;
use bio::io::gff;
use rust_htslib::bam;
use rust_htslib::bam::Read;
use std::path::Path;
fn main() -> std::io::Result<()> {
    let args: Vec<_> = std::env::args().collect();
    let mut buffer = bam::IndexedReader::from_path(&Path::new(&args[1])).unwrap();
    //buffer.set_threads(12).unwrap();
    let header = buffer.header().clone();
    let genes: Vec<_> = gff::Reader::from_file(&Path::new(&args[2]), gff::GffType::GFF3)?
        .records()
        .filter_map(|e| e.ok())
        .filter(|e| e.feature_type() == "mRNA")
        .filter_map(|e| match e.attributes().get("ID") {
            Some(id) => Some((id.clone(), e)),
            None => None,
        })
        .collect();
    let name = args[3].clone();
    eprintln!("Started:{}", name);
    for (id, record) in genes {
        let tid = header.tid(record.seqname().as_bytes()).unwrap();
        let start = *record.start() as u32 - 1; // For conversion of 1-based to 0-based.
        let end = *record.end() as u32 - 1; // The same as above
        assert!(start <= end);
        let length = end - start;
        buffer.fetch(tid, start, end).unwrap();
        let count = buffer.records().filter_map(|e| e.ok()).count();
        buffer.fetch(tid, start, end).unwrap();
        let total_depth: u32 = buffer
            .pileup()
            .filter_map(|e| e.ok())
            .map(|p| p.depth())
            .sum();
        println!("{}\t{}\t{}\t{}\t{}", name, id, total_depth, count, length);
        // println!("Currently Processing genes {}.",record.feature_type());
        // println!("Start:{}:{}-{} (0-based)",record.seqname(),start,end);
        // println!("ReadID\tAlignStart\tAlignEnd\tAlignLen\tReadLen\tMAPQ");
        // for record in buffer.records().filter_map(|e|e.ok()){
        //     let pos = record.pos() as usize - 1; // Because sam POS is 1-based.
        //     let (start,end) = get_range(pos,&record.cigar());
        //     let query_length = query_length(&record.cigar());
        //     println!("{}\t{}\t{}\t{}\t{}\t{}",String::from_utf8_lossy(record.qname()),
        //              start,end,end-start,query_length,record.mapq())
        // }
    }
    Ok(())
}

use rust_htslib::bam::record::CigarStringView;

pub fn get_range(pos: usize, cigar: &CigarStringView) -> (usize, usize) {
    // Return the position of the genome
    use rust_htslib::bam::record::Cigar::*;
    let len: usize = cigar
        .iter()
        .map(|op| match *op {
            Equal(b) | Match(b) => b as usize,
            Ins(_) | SoftClip(_) | HardClip(_) | Pad(_) => 0,
            Del(b) | RefSkip(b) | Diff(b) => b as usize,
        })
        .sum::<usize>();
    (pos, pos + len)
}

pub fn query_length(cigar: &CigarStringView) -> usize {
    use rust_htslib::bam::record::Cigar::*;
    cigar
        .iter()
        .map(|e| match e {
            HardClip(b) | SoftClip(b) | Equal(b) | Match(b) | Diff(b) | Ins(b) => *b as usize,
            _ => 0,
        })
        .sum::<usize>()
}
