#!/bin/awk -f 
## Convert input tsv to latex table(fixed column)
BEGIN{
    OFS="& ";
    OFMT="%.3f";
    print "\\begin{table}[h]";
}
NR==1{
    COLUMNS=substr("llllllll",1,NF-1)
    print "\\begin{tabular}{r"COLUMNS"} \\toprule";
    ORS="\\\\  \\midrule \n";
    gsub(/\_/,"")
    print $1,$2,$4,$5,$6,$7;
}
NR!=1{
    ORS=" \\\\\n";
    gsub(/\_/,".",$1);
    print "\\textit{"$1"}",1*$2,1*$3,1*$5,1*$6,$7*10000"e-4";
}

END{
    ORS="\n";
    print "\\bottomrule";
    print "\\end{tabular}";
    print "\\caption{Caption}";
    print "\\label{tab:rs2-subreads}";
    print "\\end{table}";
}

    
