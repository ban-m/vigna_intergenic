#!/bin/bash
#$ -S /bin/bash
#$ -N MtoBam
#$ -cwd
#$ -pe smp 24
#$ -e ./log
#$ -o ./out
#$ -V
#$ -m e

set -ue

function to_bam_and_index () {
    samtools view -hbS $1 | samtools sort -O BAM -m 10G -@2 > $2.bam
    samtools index $2.bam
}
export -f to_bam_and_index

find /grid/ban-m/vigna_species/mapped_sampled_reads/ -name *.sam | parallel to_bam_and_index {} {.} 
